import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'
import { HelpController } from './help.controller';
import { Help } from './help.entity';
import { HelpService } from './help.service';

@Module({
  imports: [TypeOrmModule.forFeature([Help])],
  controllers: [HelpController],
  providers: [HelpService],
})
export class HelpModule {}
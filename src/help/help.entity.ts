import { HelpCategory } from 'src/help-category/help-category.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';

@Entity('krs_help')
export class Help {
	
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: false ,name: "topic" })
  topic: string;

  @Column({ default: false ,name: "description" })
  description: string;

  @Column({ default: true ,name: "is_publish" })
  isPublish: boolean;
    
  @ManyToOne(type => HelpCategory, helpCategory => helpCategory.id)
  @JoinColumn({name: "id_help_category"})
  helpCategory: HelpCategory;
}
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Help } from './help.entity';

@Injectable()
export class HelpService {
  constructor(
    @InjectRepository(Help)
    private helpRepository: Repository<Help>,
  ) {}

  async findAll(): Promise<Help[]> {
    return await this.helpRepository.find();
  }

  async createHelp(data: Help): Promise<Help>{
    return await this.helpRepository.save(data);
  }

  async findOneHelp(id: string) {
    return this.helpRepository.findOneOrFail(id);
  };

  async updateHelp(id: string, help: Help) {
    return this.helpRepository.save({ ...help, id: Number(id) });
  };

  async removeHelp(id: string) {
    await this.helpRepository.findOneOrFail(id);
    return this.helpRepository.delete(id);
  };
}
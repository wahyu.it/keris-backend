import { Controller, Get, Post, Body, HttpStatus, Put, Param, Delete} from '@nestjs/common';
import { Help } from './help.entity';
import { HelpService } from './help.service';

@Controller('help')
export class HelpController {
  constructor(
	private readonly helpService: HelpService,
    ) {}

  @Get()
  async findAll() {
    return  {
      statusCode: HttpStatus.OK,
      data: await this.helpService.findAll()
    };
  }

  @Post()
  async create(@Body() data: Help)  {
	await this.helpService.createHelp(data);
    return {
      		statusCode: HttpStatus.OK,
      		message: 'Data has successfully created!',
            data: data
		};
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
	await this.helpService.findOneHelp(id);
	return
/*    return {
      statusCode: HttpStatus.OK,
      data: await this.kerisCategoryService.findOneKeris(id)
    };*/
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() help: Help) {
    return {
      statusCode: HttpStatus.OK,
      message: 'update help with the id: '+ id + ' successfully',
      data: await this.helpService.updateHelp(id, help)
    };
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return {
      statusCode: HttpStatus.OK,
      message: 'Delete help with the id: '+ id + ' successfully',
      data: await this.helpService.removeHelp(id)
    };
  }
}

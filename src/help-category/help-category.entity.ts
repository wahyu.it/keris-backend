import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('krs_help_category')
export class HelpCategory {
	
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nama: string;

}
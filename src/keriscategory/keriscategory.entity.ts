import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('krs_keris_kategory')
export class KerisCategory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  age: number;

  @Column({ default: true })
  breed: string;
}
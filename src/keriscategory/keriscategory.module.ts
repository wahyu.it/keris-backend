import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'
import { KerisCategoryListWrapper } from 'src/responese/kerisCategoryListWrapper';
import { KerisCategory } from './keriscategory.entity';
import { KerisCategoryService } from './keriscategory.service';
import { KerisCategoryController } from './keriskategory.controller';

@Module({
  imports: [TypeOrmModule.forFeature([KerisCategory])],
  controllers: [KerisCategoryController],
  providers: [KerisCategoryService, KerisCategoryListWrapper],
})
export class KerisCategoryModule {}
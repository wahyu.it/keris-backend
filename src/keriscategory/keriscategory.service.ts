import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { KerisCategory } from './keriscategory.entity';

@Injectable()
export class KerisCategoryService {
  constructor(
    @InjectRepository(KerisCategory)
    private kerisKategoryRepository: Repository<KerisCategory>,
  ) {}

  async createcategory(data: KerisCategory): Promise<KerisCategory>{
    return await this.kerisKategoryRepository.save(data);
  }

  async findAll(): Promise<KerisCategory[]> {
    return await this.kerisKategoryRepository.find();
  }

  async findOneKeris(id: string) {
    return this.kerisKategoryRepository.findOneOrFail(id);
  };

  async updateKeris(id: string, kerisCategory: KerisCategory) {
    return this.kerisKategoryRepository.save({ ...kerisCategory, id: Number(id) });
  };

  async removeKeris(id: string) {
    await this.kerisKategoryRepository.findOneOrFail(id);
    return this.kerisKategoryRepository.delete(id);
  };
}
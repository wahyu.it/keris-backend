import { Controller, Get, Post, Body, HttpStatus, Put, Param, Delete} from '@nestjs/common';
import { KerisCategoryListWrapper } from 'src/responese/kerisCategoryListWrapper';
import { KerisCategory } from './keriscategory.entity';
import { KerisCategoryService } from './keriscategory.service';

@Controller('keris-category')
export class KerisCategoryController {
  constructor(
	private readonly kerisCategoryService: KerisCategoryService,
	private readonly kerisCategoryListWrapper: KerisCategoryListWrapper,
    ) {}

  @Get()
  async findAll() {
    return  {
      statusCode: HttpStatus.OK,
      data: await this.kerisCategoryService.findAll()
    };
  }

  @Post()
  async create(@Body() data: KerisCategory)  {
	await this.kerisCategoryService.createcategory(data);
    return this.kerisCategoryListWrapper.CompleteCreate(data);
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
	await this.kerisCategoryService.findOneKeris(id);
	return
/*    return {
      statusCode: HttpStatus.OK,
      data: await this.kerisCategoryService.findOneKeris(id)
    };*/
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() kerisCategory: KerisCategory) {
    return {
      statusCode: HttpStatus.OK,
      message: 'update the keris with the id: '+ id + ' successfully',
      data: await this.kerisCategoryService.updateKeris(id, kerisCategory)
    };
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return {
      statusCode: HttpStatus.OK,
      message: 'Delete the keris with the id: '+ id + ' successfully',
      data: await this.kerisCategoryService.removeKeris(id)
    };
  }
}

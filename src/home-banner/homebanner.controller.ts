import { Body, Controller, Delete, Get, HttpStatus, NotAcceptableException, Param, Post, Put, UploadedFile, UseInterceptors } from "@nestjs/common";
import { FileInterceptor } from "@nestjs/platform-express";
import { HomeBanner } from "./homebanner.entity";
import { HomeBannerService } from "./homebanner.service";
import { diskStorage } from 'multer';
import { editFileName, imageFileFilter } from "./file-upload.utils";


@Controller('home-banner')
export class HomeBannerController {
  constructor(
	private readonly homeBannerService: HomeBannerService
	) {}

  @Get()
  async findAll() {
    return  {
      statusCode: HttpStatus.OK,
      data: await this.homeBannerService.findAll()
    };
  }

  @Post()
  @UseInterceptors(FileInterceptor('imageUrlFull', {
      storage: diskStorage({
        destination: 'D:/upload/',
        filename: editFileName
      }), 
      fileFilter: imageFileFilter
    }))

  async create( @UploadedFile() file, @Body() data: HomeBanner)  {
	await this.homeBannerService.createHomeBanner(data, file);
	console.log(file.originalname);
	console.log(file)
    return {
      		statusCode: HttpStatus.OK,
      		message: 'Data has successfully created!',
            data: data
		};
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
	await this.homeBannerService.findOneHomeBanner(id);
	return
/*    return {
      statusCode: HttpStatus.OK,
      data: await this.kerisCategoryService.findOneKeris(id)
    };*/
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() homeBanner: HomeBanner) {
    return {
      statusCode: HttpStatus.OK,
      message: 'update the keris with the id: '+ id + ' successfully',
      data: await this.homeBannerService.updateHomeBanner(id, homeBanner)
    };
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return {
      statusCode: HttpStatus.OK,
      message: 'Delete the keris with the id: '+ id + ' successfully',
      data: await this.homeBannerService.removeHomeBanner(id)
    };
  }
}

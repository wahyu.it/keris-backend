import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { HomeBanner } from "./homebanner.entity";

@Injectable()
export class HomeBannerService {
  constructor(
    @InjectRepository(HomeBanner)
    private homeBannerRepository: Repository<HomeBanner>,
  ) {}

  async createHomeBanner(data: HomeBanner, file: any): Promise<HomeBanner>{
	console.log('create home banner :', data, file.originalname );
	const test ={
		  title: data.title,
		  subTitle: data.subTitle,
		  imageUrlFull: file.path,
		  imageUrlThumb: data.imageUrlThumb,
		  isPublished: data.isPublished,
		  ordering: data.ordering
	}
    return await this.homeBannerRepository.save(test);
  }

  async findAll(): Promise<HomeBanner[]> {
    return await this.homeBannerRepository.find();
  }

  async findOneHomeBanner(id: string) {
    return this.homeBannerRepository.findOneOrFail(id);
  };

  async updateHomeBanner(id: string, homeBanner: HomeBanner) {
    return this.homeBannerRepository.save({ ...homeBanner, id: Number(id) });
  };

  async removeHomeBanner(id: string) {
    await this.homeBannerRepository.findOneOrFail(id);
    return this.homeBannerRepository.delete(id);
  };
}
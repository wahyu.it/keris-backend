import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('krs_home_banner')
export class HomeBanner {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: false ,name: "title" })
  title: string;

  @Column({ default: false ,name: "sub_title" })
  subTitle: string;

  @Column({ default: false ,name: "image_url_full" })
  imageUrlFull: string;

  @Column({ default: false ,name: "image_url_thumb" })
  imageUrlThumb: string;

  @Column({ default: true ,name: "is_published" })
  isPublished: boolean;

  @Column({ default: false ,name: "ordering" })
  ordering: number;
}
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { HomeBannerController } from "./homebanner.controller";
import { HomeBanner } from "./homebanner.entity";
import { HomeBannerService } from "./homebanner.service";

@Module({
  imports: [TypeOrmModule.forFeature([HomeBanner])],
  controllers: [HomeBannerController],
  providers: [HomeBannerService],
})
export class HomeBannerModule {}
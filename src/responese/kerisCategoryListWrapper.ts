import { HttpStatus } from "@nestjs/common";

export class KerisCategoryListWrapper {
	
	public CompleteCreate(data: any) {
		return {
      		statusCode: HttpStatus.OK,
      		message: 'Keris Category has successfully created!',
            data: data
		}
	}

}
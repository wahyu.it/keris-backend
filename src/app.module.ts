import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KerisCategory } from './keriscategory/keriscategory.entity';
import { KerisCategoryModule } from './keriscategory/keriscategory.module';
import { Connection } from 'typeorm';
import { HomeBanner } from './home-banner/homebanner.entity';
import { HomeBannerModule } from './home-banner/homebanner.module';
import { HelpModule } from './help/help.module';
import { Help } from './help/help.entity';
import { HelpCategory } from './help-category/help-category.entity';

@Module({
  imports: [TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'keris_db',
      entities: [KerisCategory, HomeBanner, Help, HelpCategory],
      synchronize: true,
    }), KerisCategoryModule, HomeBannerModule, HelpModule],
  controllers: [],
  providers: [],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
